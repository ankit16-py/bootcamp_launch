# Copyright 2021 the Autoware Foundation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Co-developed by Tier IV, Inc. and Apex.AI, Inc.

"""
Example launch file for a new package.

Note: Does not work in ROS2 dashing!
"""

import os

from ament_index_python import get_package_share_directory
import launch.substitutions
from launch_ros.actions import Node
from launch import LaunchDescription


def get_shared_file(package_name, folder_name, file_name):
    """Pass the given param file as a LaunchConfiguration."""
    file_path = os.path.join(
        get_package_share_directory(package_name),
        folder_name,
        file_name)
    return file_path


def generate_launch_description():
    """Generate launch description with a single component."""
    dataspeed_ford_dbw = Node(
        executable='dbw_node',
        name='dataspeed_ford_dbw_node',
        namespace='dbw',
        package='dbw_ford_can',
        parameters=[launch.substitutions.LaunchConfiguration(
            'params',
            default=get_shared_file('bootcamp_launch',
                                    'config',
                                    'dbw_params.yaml'))],
        output='screen'
        )

    with open(get_shared_file('bootcamp_launch', 'urdf', 'lincoln_mkz.urdf'), 'r') as infp:
        urdf_file = infp.read()

    urdf_publisher = Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        name='robot_state_publisher',
        parameters=[{'robot_description': urdf_file}],
    )

    return LaunchDescription([dataspeed_ford_dbw, urdf_publisher])
